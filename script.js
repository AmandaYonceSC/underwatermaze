//assigning canvas layers to variables

//blue gradient background layer
let background=document.getElementById("layer1")
let backContext=background.getContext("2d")
    background.width = 1200
    background.height = 750

//bubbles layer
let canvas=document.getElementById("layer2")
let ctx = canvas.getContext("2d")
    canvas.width=1200
    canvas.height=750

//seaweed & coral layer
let bottom = document.getElementById("layer3")
let bottomContext = bottom.getContext("2d")
    bottom.width=1200
    bottom.height=750

//fish layer
let fishLayer = document.getElementById("layer4")
let fishContext = fishLayer.getContext("2d")
    fishLayer.width=1200
    fishLayer.height=750

//create Green Fish
let fish=new Image()
    fish.src="./fishSprite.png"
    fish.onload=()=>{
        fishContext.drawImage(fish, 0, 0, 0, 0, 0, 0, 0, 0)
    }

//create yellow fish
let schoolLayer = document.getElementById("layer5")
let schoolContext = schoolLayer.getContext("2d")
    schoolLayer.width=1200
    schoolLayer.height=750
let school= new Image()
    school.src="./yellowFish.png"
    school.onload=()=>{
        schoolContext.drawImage(school, 0, 0, 0, 0, 0, 0, 0, 0)
    }

//create seaweed and coral bottom    
    let coral=new Image()
        coral.src="./coral.png"
        coral.onload=()=>{
        bottomContext.drawImage(coral, 750, 480)
        }
    let seaweed=new Image()
        seaweed.src="./seagrass.png"
        seaweed.onload=()=>{
        bottomContext.drawImage(seaweed, -50, 625)
        bottomContext.drawImage(coral, 45, 590, 125, 160)
        bottomContext.drawImage(coral, 950, 570, 170, 200)
        bottomContext.drawImage(seaweed, 1000, 580, 200, 200)
        }
    let coralMountainImage= new Image()
        coralMountainImage.src="./coralMountain.png"
        coralMountainImage.onload=()=>{
        bottomContext.drawImage(coralMountainImage, 200, 310)
        bottomContext.drawImage(seaweed, 60, 565, 200, 200)
        bottomContext.drawImage(seaweed, 520, 500, 280, 280)
        }

//draws background with gradient
function drawBackground(){
    let gradient=backContext.createRadialGradient(
        background.width/2, 5, 
        20, 
        background.width/2, 100, 
        800)
    gradient.addColorStop(0.05, "#22a4ff")
    gradient.addColorStop(0.15, "#2278ff")
    gradient.addColorStop(0.25, "#224cff")
    gradient.addColorStop(0.35, "#2422ff")
    gradient.addColorStop(0.75, "#1c3fd4")
    gradient.addColorStop(1, "#1733aa")
    backContext.fillStyle=gradient
    backContext.fillRect(0, 0, background.width, background.height)
    
}
drawBackground()

//bubbles Object and array to store bubbles once created
let bubbles=[]

function Bubble(width, height, xPos, yPos, rightLeft, speed){
    this.rightLeft=rightLeft
    this.speed=speed
    this.width=width
    this.height= height
    this.xPos=xPos
    this.yPos=yPos 
}

//create 1000 Bubbles with random size, random speed and random start location
function createBubbles(){
    for(let i=0; i<1000; i++){
        let randomX = Math.round(-200 + Math.random() *1350)
        let randomY = Math.round(-200 + Math.random() * 1100)
        let size = 5 + Math.random() *10
        let rightLeft= Math.round(-2 + Math.random() *4.5)
        let speed= Math.round(-20 + Math.random() +20)
        let bubble = new Bubble(size, size, randomX, randomY, rightLeft, speed)
        bubbles.push(bubble)
    }
}
createBubbles()

setInterval(moveBubbles, 50)

//sets bubble image Source
let image=new Image()
image.src="./bubble.png"
image.onload=()=>{
    ctx.drawImage(image, 0, 0)
}


//place and move the bubbles 
function moveBubbles(){
    ctx.clearRect(0,0,canvas.width, canvas.height)
    for(i=0; i<bubbles.length; i++){
        
        if(bubbles[i].xPos<-400){
            bubbles[i].xPos=Math.round(-200 + Math.random() *1350)
        } else if (bubbles[i].xPos>1550){
            bubbles[i].xPos=Math.round(-200 + Math.random() *1350)
        } 
        if(bubbles[i].counter%2===0){
            bubbles[i].xPos+=bubbles[i].rightLeft
        } else{
            bubbles[i].xPos-=bubbles[i].rightLeft
        } 
        bubbles[i].yPos-=5
        if (bubbles[i].yPos<0){
            bubbles[i].yPos=Math.round(800 + Math.random() * 1100)
        } 
    ctx.drawImage(image, bubbles[i].xPos, bubbles[i].yPos, bubbles[i].width, bubbles[i].height)
    }
}

    let seaLife=[]

function seaLifeObject(seaLifeName, fishXSelection, fishYSelection, fishWidth, fishHeight, 
                        fishXPosition, fishYPosition, fishXSelectionStart, fishXSelectionEnd, 
                        fishXMovement, fishYMovement, fishYBeginning, fishXBeginning, fishYMinimum, fishYMaximum,
                        fishWidthToDraw, fishHeightToDraw){
    this.seaLifeName= seaLifeName,          //image reference for drawImage function
    this.fishXSelection= fishXSelection,     //the X value of the current image sprite selection start
    this.fishYSelection= fishYSelection,     //the Y value of the current image sprite selection start 
    this.fishWidth= fishWidth,               //width of image on original sprite
    this.fishHeight= fishHeight,             //height of image on original sprite
    this.fishXPosition= fishXPosition,       //current x position of image on canvas
    this.fishYPosition= fishYPosition,       //current y position of image on canvas
    this.fishXSelectionStart= fishXSelectionStart,       //X location of image beginning on sprite
    this.fishXSelectionEnd= fishXSelectionEnd,           //X location of image ending on sprite
    this.fishXMovement=fishXMovement                     //x distance movement of image
    this.fishYMovement=fishYMovement                     //y distance movement of image
    this.fishXBeginning=fishXBeginning           //the x value for image to start movement on canvas
    this.fishYBeginning=fishYBeginning           //the y value for image to start movement on canvas
    this.fishYMinimum=fishYMinimum              //the lowest Y point that animal can go to on canvas
    this.fishYMaximum=fishYMaximum              //the highest Y point that object can go to on canvas
    this.fishWidthToDraw=fishWidthToDraw   //width to actually draw image on canvas
    this.fishHeightToDraw=fishHeightToDraw //Height to actually draw image on canvas
    
}

let leftGreenFish=new seaLifeObject(fish, 0, 512, 256, 256, 0, 139, 0, 1280, 2.5, 2.5, 0, -140, 0, 150, 200, 260)
seaLife.push(leftGreenFish)



function fishMoveLeftToRight(){
    
    fishContext.clearRect(0, 0, fishLayer.width, fishLayer.height)

    for(i=0; i<seaLife.length; i++){
        
      
    if(seaLife[i].fishXSelection<seaLife[i].fishXSelectionEnd){        //cycles through the 5 images to create swimming animation
        seaLife[i].fishXSelection+=seaLife[i].fishWidth
    } else {seaLife[i].fishXSelection=seaLife[i].fishXSelectionStart}

    seaLife[i].fishXPosition+=seaLife[i].fishXMovement   //moves the fish across the screen
    seaLife[i].fishYPosition+=seaLife[i].fishYMovement

    if(seaLife[i].fishYPosition<seaLife[i].fishYMinimum){
        seaLife[i].fishYMovement=seaLife[i].fishYMovement*-1
    } else if(seaLife[i].fishYPosition>seaLife[i].fishYMaximum){
        seaLife[i].fishYMovement=seaLife[i].fishYMovement*-1
   }

    if(seaLife[i].fishXPosition>1250 || seaLife[i].fishXPosition<-150){     //sends fish back to start once it exits the screen on the right
        seaLife[i].fishXPosition=seaLife[i].fishXBeginning
        seaLife[i].fishYPosition=seaLife[i].fishYBeginning
    }
    fishContext.drawImage(seaLife[i].seaLifeName, seaLife[i].fishXSelection, seaLife[i].fishYSelection, seaLife[i].fishWidth, 
        seaLife[i].fishHeight, seaLife[i].fishXPosition, seaLife[i].fishYPosition, seaLife[i].fishWidthToDraw, seaLife[i].fishHeightToDraw)
}
}

setInterval(fishMoveLeftToRight, 80)



//array and object for school of yellow fish
let schoolFish=[]

function schoolFishes(seaLifeName, fishXSelection, fishYSelection, fishWidth, fishHeight, 
    fishXPosition, fishYPosition, fishXSelectionStart, fishXSelectionEnd, 
    fishXMovement, fishYMovement, fishYBeginning, fishXBeginning, fishYMinimum, fishYMaximum,
    fishWidthToDraw, fishHeightToDraw){
    this.seaLifeName= seaLifeName,          //image reference for drawImage function
    this.fishXSelection= fishXSelection,     //the X value of the current image sprite selection start
    this.fishYSelection= fishYSelection,     //the Y value of the current image sprite selection start 
    this.fishWidth= fishWidth,               //width of image on original sprite
    this.fishHeight= fishHeight,             //height of image on original sprite
    this.fishXPosition= fishXPosition,       //current x position of image on canvas
    this.fishYPosition= fishYPosition,       //current y position of image on canvas
    this.fishXSelectionStart= fishXSelectionStart,       //X location of image beginning on sprite
    this.fishXSelectionEnd= fishXSelectionEnd,           //X location of image ending on sprite
    this.fishXMovement=fishXMovement                     //x distance movement of image
    this.fishYMovement=fishYMovement                     //y distance movement of image
    this.fishXBeginning=fishXBeginning           //the x value for image to start movement on canvas
    this.fishYBeginning=fishYBeginning           //the y value for image to start movement on canvas
    this.fishYMinimum=fishYMinimum              //the lowest Y point that animal can go to on canvas
    this.fishYMaximum=fishYMaximum              //the highest Y point that object can go to on canvas
    this.fishWidthToDraw=fishWidthToDraw   //width to actually draw image on canvas
    this.fishHeightToDraw=fishHeightToDraw //Height to actually draw image on canvas
}

//create a school of Yellow Fish to move from right to left
function createSchool(){
    for(let i=0; i<10; i++){
        let name=school
        let XS=581.875
        let XY=0
        let FW=83.125
        let FH=54
        let FXP= 1100 + Math.random() *1350
        let FYP= 100 + Math.random() *300
        let FXSS=581.875
        let FXSE=82.125
        let FXM=1 + Math.random() *3
        let FXY=1 + Math.random() *3
        let FXB = 1350
        let FYB = 300
        let FYMin=150
        let FYMax=500
        let FWD=30+ Math.random() *100
        let FHD= .63*FWD

        let fishForSchool = new schoolFishes(name, XS, XY, FW, FH, FXP, FYP, FXSS, FXSE, FXM, FXY, FYB, FXB, FYMin, FYMax, FWD, FHD )
        schoolFish.push(fishForSchool)
    }
}

createSchool()


fishMoveRightToLeft()
function fishMoveRightToLeft(){
    
    schoolContext.clearRect(0, 0, schoolLayer.width, schoolLayer.height)

    for(i=0; i<schoolFish.length; i++){
        console.log(schoolFish)
      
    if(schoolFish[i].fishXSelection>schoolFish[i].fishXSelectionEnd){        //cycles through the sprite images to create swimming animation
        schoolFish[i].fishXSelection-=schoolFish[i].fishWidth
    } else {schoolFish[i].fishXSelection=schoolFish[i].fishXSelectionStart}

    schoolFish[i].fishXPosition-=schoolFish[i].fishXMovement   //moves the fish across the screen
    schoolFish[i].fishYPosition+=schoolFish[i].fishYMovement

    if(schoolFish[i].fishYPosition<schoolFish[i].fishYMinimum){
        schoolFish[i].fishYMovement=schoolFish[i].fishYMovement*-1
    } else if(schoolFish[i].fishYPosition>schoolFish[i].fishYMaximum){
        schoolFish[i].fishYMovement=schoolFish[i].fishYMovement*-1
   }

    if(schoolFish[i].fishXPosition<-100){     //sends fish back to start once it exits the screen on the right
        schoolFish[i].fishXPosition=schoolFish[i].fishXBeginning
        schoolFish[i].fishYPosition=schoolFish[i].fishYBeginning
    }
    fishContext.drawImage(schoolFish[i].seaLifeName, schoolFish[i].fishXSelection, schoolFish[i].fishYSelection, schoolFish[i].fishWidth, 
        schoolFish[i].fishHeight, schoolFish[i].fishXPosition, schoolFish[i].fishYPosition, schoolFish[i].fishWidthToDraw, schoolFish[i].fishHeightToDraw)
}

requestAnimationFrame(fishMoveRightToLeft)
}

//setInterval(fishMoveRightToLeft, 120)
